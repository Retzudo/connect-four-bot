import logging
import os
from itertools import cycle

import discord

from game import Game

COMMAND_PREFIX = os.getenv("CONNECT_FOUR_BOT_COMMAND_PREFIX", "$")

logging.basicConfig(level=logging.DEBUG)
client = discord.Client()


def is_command(message: discord.Message) -> bool:
    return message.server and message.content.startswith(COMMAND_PREFIX)


@client.event
async def on_ready():
    logging.info("Logged in")


@client.event
async def on_message(message: discord.Message):
    if not is_command(message):
        return

    if message.content.startswith("{}connectfour".format(COMMAND_PREFIX)):
        first_player: discord.Member = message.author

        if len(message.mentions) > 0:
            other_player: discord.Member = message.mentions[0]
        else:
            await client.send_message(message.channel, "Who do you want to play with?")
            response: discord.Message = await client.wait_for_message(
                timeout=60.0,
                author=message.author,
                channel=message.channel,
                check=lambda m: len(m.mentions) > 0,
            )

            if not response:
                await client.send_message(message.channel, "You didn't mention anybody in time. Aborting game.")
                return

            other_player: discord.Member = response.mentions[0]

        if other_player.bot:
            await client.send_message(message.channel, "You can't play with bots. Aborting game.")
            return

        await client.send_message(
            message.channel,
            "<@{}>, do you want to play Connect Four with <@{}>? Type `yes` if you want to.".format(
                other_player.id, first_player.id
            ),
        )
        response: discord.Message = await client.wait_for_message(
            timeout=60.0,
            author=other_player,
            channel=message.channel
        )

        if not response:
            await client.send_message(message.channel, "<@{}> did not respond in time. Aborting game.".format(other_player.id))
            return

        if response.content.lower() != 'yes':
            await client.send_message(message.channel, "You didn't type `yes`. Aborting game.")
            return

        await client.send_message(
            message.channel,
            "Okay, here we go <@{}> (:red_circle:) and <@{}> (:large_blue_circle:)!".format(
                first_player.id, other_player.id
            ),
        )

        await play_game(first_player, other_player, message.channel)


async def play_game(
    player_one: discord.Member, player_two: discord.Member, channel: discord.Channel
):
    game: Game = Game(player_one.id, player_two.id)
    turns = cycle([player_two, player_one])
    reactions = ["1⃣", "2⃣", "3⃣", "4⃣", "5⃣", "6⃣", "7⃣"]
    reaction_timeout = 120.0

    message: discord.Message = await client.send_message(channel, "Connect Four!")

    while not game.player_has_won():
        await client.clear_reactions(message)
        turn: discord.Member = next(turns)
        game_string = await render_game(game)
        game_string += "\nIt's <@{}>'s turn. In what column would you like to drop your piece? (Please wait until all 7 buttons have appeared)".format(
            turn.id
        )
        await client.edit_message(message, game_string)
        for reaction in reactions:
            await client.add_reaction(message, reaction)

        response = await client.wait_for_reaction(
            reactions, user=turn, timeout=reaction_timeout, message=message
        )

        if not response:
            await client.send_message(
                channel,
                "<@{}> did not make their move in time ({:g}s). Aborting game.".format(
                    turn.id, reaction_timeout
                ),
            )
            return

        game.place_piece(turn.id, reactions.index(response.reaction.emoji))

    game_string = await render_game(game)
    game_string += "\nCongratulations <@{}>! You won!".format(turn.id)
    await client.edit_message(message, game_string)
    await client.clear_reactions(message)


async def render_game(game: Game) -> str:
    lines = []
    game.print()
    for ver in range(game.height):
        line = ""
        for hor in range(game.width):
            piece = game.piece_at(hor, game.height - 1 - ver)
            if piece == game.player_one:
                line += ":red_circle:"
            elif piece == game.player_two:
                line += ":large_blue_circle:"
            else:
                line += ":black_circle:"

        lines.append(line)

    return "\n".join(lines)


if __name__ == "__main__":
    token = os.getenv("CONNECT_FOUR_BOT_TOKEN")

    if not token:
        logging.critical("Token not specified. Cannot run.")
        exit(1)

    client.run(token)
