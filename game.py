from collections import deque
from typing import Optional
from itertools import zip_longest


class Game:
    """A game of Connect Four between two players."""

    def __init__(
        self,
        player_one: int,
        player_two: int,
        width: int = 7,
        height: int = 6,
        pieces_to_win: int = 4,
    ) -> None:
        self.player_two = player_two
        self.player_one = player_one

        self.width = width
        self.height = height
        self.pieces_to_win = pieces_to_win
        self.field = [[] for _ in range(width)]

    def place_piece(self, player_id: int, lane_number: int) -> bool:
        """Place in the given lane.

        Return True if the piece was placed.
        Return False if the piece was not placed, i. e. when the lane is full."""
        if 0 > lane_number >= self.width:
            raise ValueError(
                "Position {} lies outside the playing field which is {} long".format(
                    lane_number, self.width
                )
            )

        if player_id not in (self.player_one, self.player_two):
            raise ValueError("Invalid player ID {}".format(player_id))

        lane = self.field[lane_number]

        if len(lane) >= self.height:
            return False

        lane.append(player_id)

        return True

    def _check_list_for_win(self, l) -> Optional[int]:
        if len(l) < self.pieces_to_win:
            return

        if len(l) == self.pieces_to_win and any(l):
            if len(set(l)) == 1:
                return l[0]

        for i in range(len(l) - self.pieces_to_win + 1):
            if len(set(l[i: i + self.pieces_to_win])) == 1 and l[i] is not None:
                return l[i]

    def player_has_won(self) -> Optional[int]:
        """Check whether a player has won.

        Return the player ID if a player has won or None otherwise."""
        # 1. Check vertically
        for lane in self.field:
            winner = self._check_list_for_win(lane)
            if winner:
                return winner

        # 2. Flip the field to check horizontally
        flipped_field = zip_longest(*self.field)
        for lane in flipped_field:
            winner = self._check_list_for_win(lane)
            if winner:
                return winner

        # 3. Check right diagonals
        flipped_field = list(zip_longest(*self.field))
        rotated = []
        # Rotate each lane by one more than the last to line up the pieces
        for i, lane in enumerate(flipped_field):
            d = deque(lane)
            d.rotate(-i)
            rotated.append(list(d))
        # Flip it back and check for wins
        rotated_field = list(zip_longest(*rotated))
        self.print(rotated_field)

        for lane in rotated_field:
            winner = self._check_list_for_win(lane)
            if winner:
                return winner

        # 4. Check left diagonals
        flipped_field = list(zip_longest(*self.field))
        rotated = []
        # Rotate each lane by one more than the last to line up the pieces
        for i, lane in enumerate(flipped_field):
            d = deque(lane)
            d.rotate(i)
            rotated.append(list(d))
        # Flip it back and check for wins
        rotated_field = list(zip_longest(*rotated))
        self.print(rotated_field)

        for lane in rotated_field:
            winner = self._check_list_for_win(lane)
            if winner:
                return winner

    def fill(self):
        """Fill the field with pieces (for testing)."""
        import random

        for lane_number in range(self.width):
            while True:
                placed = self.place_piece(
                    random.choice([self.player_one, self.player_two]), lane_number + 1
                )
                if not placed:
                    break

    def piece_at(self, horizontal_index: int, vertical_index: int) -> Optional[int]:
        if 0 > horizontal_index >= self.width:
            raise IndexError(
                "Horizontal index {} is out of range.".format(horizontal_index)
            )

        if 0 > vertical_index >= self.height:
            raise IndexError(
                "Vertical index {} is out of range.".format(vertical_index)
            )

        try:
            return self.field[horizontal_index][vertical_index]
        except IndexError:
            return None

    def print(self, field=None):
        field = field or self.field

        for h in range(self.height, 0, -1):
            for lane in field:
                try:
                    c = str(lane[h-1])
                    if c == 'None':
                        c = ' '
                    print(c + ' ', end='')
                except IndexError:
                    print('  ', end='')
            print()
