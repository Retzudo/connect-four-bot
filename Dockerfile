FROM python:3.6-alpine

RUN pip install pipenv

WORKDIR /bot
COPY Pipfile* /bot/
RUN pipenv install --system

COPY ./*.py /bot/

CMD python /bot/bot.py
