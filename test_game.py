from game import Game


def test_random_pattern():
    g = Game(1, 2)

    g.place_piece(1, 0)
    g.place_piece(2, 1)
    g.place_piece(1, 2)
    g.place_piece(2, 3)
    g.place_piece(1, 4)
    g.place_piece(2, 5)
    g.place_piece(1, 6)

    assert g.player_has_won() is None


def test_vertical():
    g = Game(1, 2)

    g.place_piece(1, 0)
    g.place_piece(1, 0)
    g.place_piece(1, 0)
    g.place_piece(1, 0)

    assert g.player_has_won() == 1


def test_horizontal():
    g = Game(1, 2)

    g.place_piece(1, 0)
    g.place_piece(1, 1)
    g.place_piece(1, 2)
    g.place_piece(1, 3)

    assert g.player_has_won() == 1


def test_right_diagonal():
    g = Game(1, 2)

    g.place_piece(2, 1)
    g.place_piece(2, 2)
    g.place_piece(2, 3)

    g.place_piece(2, 2)
    g.place_piece(2, 3)

    g.place_piece(2, 3)

    g.place_piece(1, 0)
    g.place_piece(1, 1)
    g.place_piece(1, 2)
    g.place_piece(1, 3)

    assert g.player_has_won() == 1


def test_left_diagonal():
    g = Game(1, 2)

    g.place_piece(2, 1)
    g.place_piece(2, 2)
    g.place_piece(2, 3)

    g.place_piece(2, 1)
    g.place_piece(2, 2)

    g.place_piece(2, 1)

    g.place_piece(1, 1)
    g.place_piece(1, 2)
    g.place_piece(1, 3)
    g.place_piece(1, 4)

    assert g.player_has_won() == 1
